﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwait
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            var array = await AsyncAwaitClass.generateArray();
            array = await AsyncAwaitClass.multiplyArray(array);
            array = await AsyncAwaitClass.sortedArray(array);
            var average = await AsyncAwaitClass.average(array);
            Console.WriteLine();
            Console.WriteLine("Average");
            Console.WriteLine(average);
        }
    }
}
