﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwait
{
    public class AsyncAwaitClass
    {
        public static async Task<int[]> generateArray()
        {
            Random rn = new Random();
            int[] randomNumbers = new int[10];
            for (int i = 0; i < 10; i++)
            {
                randomNumbers[i] = rn.Next();
            }

            Console.WriteLine("Generated array");
            foreach (int item in randomNumbers)
            {
                Console.Write(item + " ");
            }

            return randomNumbers;
        }

        public static async Task<int[]> multiplyArray(int[] numberArray, int myRandom = 0)
        {
            Validation(numberArray);

            Random rn = new Random();
            int randomNumber;
            if (myRandom != 0)
            {
                randomNumber = myRandom;
            }
            else
            {
                randomNumber = rn.Next();
            }

            for (int i = 0; i < 10; i++)
            {
                numberArray[i] = numberArray[i] * randomNumber;
            }

            Console.WriteLine();
            Console.WriteLine("Multipled array");
            foreach (int item in numberArray)
            {
                Console.Write(item + " ");
            }

            return numberArray;
        }

        public static async Task<int[]> sortedArray(int[] numberArray)
        {
            Validation(numberArray);
            Array.Sort(numberArray);
            Console.WriteLine();
            Console.WriteLine("Sorted array");
            foreach (int item in numberArray)
            {
                Console.Write(item + " ");
            }

            return numberArray;
        }

        public static async Task<double> average(int[] numberArray)
        {
            Validation(numberArray);
            return numberArray.Average();
        }

        public static void Validation(int[] numberArray)
        {
            if (numberArray.Length == 0)
            {
                throw new ArgumentException("Array can't be empty", nameof(numberArray));
            }

            if (numberArray.Length > 10)
            {
                throw new ArgumentOutOfRangeException(nameof(numberArray));
            }
        }
    }
}
