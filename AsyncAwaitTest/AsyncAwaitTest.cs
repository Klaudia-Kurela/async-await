using NUnit.Framework;
using System;
using AsyncAwait;

namespace AsyncAwaitTest
{
    public class Tests
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        private Random rn;
        private int[] array;
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        [SetUp]
        public void Setup()
        {
            rn = new Random();
            array = AsyncAwaitClass.generateArray();
        }

        [Test]
        public void GenerateArrayTest()
        {
            var expectedLength = 10;
            Assert.AreEqual(expectedLength, array.Length);
        }

        [Test]
        public void MultiplyArrayTest()
        {
            var randomNumber = rn.Next();
            int[] newArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            int[] expected = new int[10];
            for (int i = 0; i < 10; i++)
            {
                expected[i] = newArray[i] * randomNumber;
            }

            var multiplayed = AsyncAwaitClass.multiplyArray(newArray, randomNumber);
            Assert.AreEqual(expected, multiplayed);
        }

        [Test]
        public void MultiplyArrayEmptyTest()
        {
            Assert.Throws<ArgumentException>(
                () => AsyncAwaitClass.multiplyArray(Array.Empty<int>()),
                message: "Method throws ArgumentException in case an array is empty");
        }

        [Test]
        public void MultiplyArrayTooLongTest()
        {
            Assert.Throws<ArgumentOutOfRangeException>(
                () => AsyncAwaitClass.multiplyArray(new int[11]),
                message: "Method throws ArgumentException in case an array is too long");
        }

        [Test]
        public static void SortedArrayTest()
        {
            int[] newArray = { 3, 7, 5, 2, 1, 4, 9, 6, 8, 1 };
            int[] expected = { 1, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var result = AsyncAwaitClass.sortedArray(newArray);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void SortedArrayEmptyTest()
        {
            Assert.Throws<ArgumentException>(
                () => AsyncAwaitClass.sortedArray(Array.Empty<int>()),
                message: "Method throws ArgumentException in case an array is empty");
        }

        [Test]
        public void SortedArrayTooLongTest()
        {
            Assert.Throws<ArgumentOutOfRangeException>(
                () => AsyncAwaitClass.sortedArray(new int[11]),
                message: "Method throws ArgumentException in case an array is too long");
        }

        [Test]
        public static void AverageTest()
        {
            int[] newArray = { 3, 7, 5, 2, 1, 4, 9, 6, 8, 1 };
            var result = AsyncAwaitClass.average(newArray);
            Assert.AreEqual(4.6, result);
        }

        [Test]
        public void AverageEmptyTest()
        {
            Assert.Throws<ArgumentException>(
                () => AsyncAwaitClass.average(Array.Empty<int>()),
                message: "Method throws ArgumentException in case an array is empty");
        }

        [Test]
        public void AverageTooLongTest()
        {
            Assert.Throws<ArgumentOutOfRangeException>(
                () => AsyncAwaitClass.average(new int[11]),
                message: "Method throws ArgumentException in case an array is too long");
        }
    }
}